CREATE DATABASE postfixadmin CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON postfixadmin.* TO 'postfixadmin'@'%' IDENTIFIED BY 'postfixadmin';
FLUSH PRIVILEGES;
